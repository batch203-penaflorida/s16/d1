console.log("Hello World");

let x = 1397;
let y = 444;

// Add
let sum = x + y;
console.log("Result of addition operator: " + sum);
let difference = x - y;
console.log("Result of subtraction operator: " + difference);
let product = x * y;
console.log("Result of multiplication operator: " + product);
let quotient = x / y;
console.log("Result of division operator: " + quotient);
let modulo = x / y;
console.log("Result of modulo operator: " + modulo);

let assignmentNumber = 8;

console.log(
  "The current value of the assignmentNumber variable: " + assignmentNumber
);
assignmentNumber += 2;
console.log("Result of addition assigmentNumber operator: " + assignmentNumber);

assignmentNumber -= 2;

console.log("Result of addition assigmentNumber operator: " + assignmentNumber);
assignmentNumber *= 2;
console.log("Result of addition assigmentNumber operator: " + assignmentNumber);
assignmentNumber /= 2;
console.log("Result of addition assigmentNumber operator: " + assignmentNumber);
// PEMDAS (Order of Operations)
// Parenthesis Exponent Multiplication Division Addition Subtraction

let mdas = 1 + 2 - (3 * 4) / 5;
console.log(mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);

console.log(pemdas);
/* The operations were done 
1. 4 / 5 = 0.8 | 1 + (2-3)
2. 2 - 3 = -1 | 1 + (-1) * 0.8
3. -1 * 0.8 = -0.8
4. 1 - 0.8 = 0.20  result
*/
// let pemdas = 1 + (2-3) * (4/5);
// group the operators using parenthesis will deliver different result.

// Increment and Decrement
let z = 1;
let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment for z: " + z);

increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post increment for z: " + z);

//JavaScript reads from taop to bottom and from left to right

let decrement = --z;

console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);
decrement = z--;

console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

// Type Coercion
/* 
   is the automatic conversion of values from one data type to another.
*/

let numA = "10";
let numB = 12;
let coercion = numA + numB;

console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion); //30
console.log(typeof nonCoercion);
/* 
   -The result is a number
   -The boolean "true" is also associated with the value of 1.
*/
let numE = true + 1;
console.log(numE);
console.log(typeof numE);
let numF = false + 1;
console.log(numF);
console.log(typeof numF);

let isFalse = false;
console.log(isFalse);
console.log(typeof isFalse);
